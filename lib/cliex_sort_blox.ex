defmodule CliexSortBlox do
  use CliexSortBlox.Types

  alias CliexSortBlox.{Block, State}

  @typep state_t :: :start | :candidate | :inner

  @spec sort_blox(Regex.t(), Regex.t(), maybe(Regex.t()), Enum.t()) :: binaries()
  def sort_blox(srgx, ergx, prgx, input) do
    input
    |> Enum.into([])
    |> state_machine(:start, State.new(srgx, ergx, prgx) |> State.debug())
    |> Enum.sort_by(& &1.key)
    |> Block.blocks_to_output()
  end

  @spec state_machine(binaries(), state_t(), State.t()) :: Block.ts()
  defp state_machine(input, current_state, state)

  defp state_machine([line | rest], :start, state) do
    State.inspect(state, :start)

    cond do
      Regex.match?(state.ergx, line) -> add_new_block(rest, :inner, state, line)
      state.prgx && Regex.match?(state.prgx, line) -> add_new_candidate_line(rest, state, line)
      true -> state_machine(rest, :start, state)
    end
  end

  defp state_machine([line | rest], :inner, state) do
    State.inspect(state, :inner)

    cond do
      state.srgx == state.ergx && Regex.match?(state.ergx, line) ->
        add_new_block(rest, :inner, state, line)

      Regex.match?(state.ergx, line) ->
        add_new_line(rest, :start, state, line)

      state.prgx && Regex.match?(state.prgx, line) ->
        add_new_candidate_line(rest, state, line)

      true ->
        add_new_line(rest, :inner, state, line)
    end
  end

  defp state_machine([line | rest], :candidate, state) do
    State.inspect(state, :candidate)

    cond do
      state.srgx == state.ergx && Regex.match?(state.ergx, line) ->
        add_new_block(rest, :inner, state, line)

      true ->
        add_new_candidate_line(rest, state, line)
    end
  end

  defp state_machine([], _, state), do: state.blocks

  @spec add_new_block(binaries(), state_t(), State.t(), binary()) :: Block.ts()
  defp add_new_block(input, next_state, state, line) do
    state_machine(input, next_state, State.new_block(state, line))
  end

  @spec add_new_candidate_line(binaries(), State.t(), binary()) :: Block.ts()
  defp add_new_candidate_line(rest, state, line) do
    state_machine(rest, :candidate, State.add_candidate_line(state, line))
  end

  @spec add_new_line(binaries(), state_t(), State.t(), binary()) :: Block.ts()
  defp add_new_line(input, new_state, state, line) do
    state_machine(input, new_state, State.add_new_line(state, line))
  end
end

# SPDX-License-Identifier: Apache-2.0
