defmodule Test.Acceptance.DefsWithCommentTest do
  use ExUnit.Case

  import CliexSortBlox, only: [sort_blox: 4]

  @two_blox """
    # This is function b
    def b; 41 end

    # This is function a
    # with no arguments
    def a
      42
    end
  """
  describe "only one block" do
    test "no sorting" do
      result = subject(@one_block |> String.split("\n")) |> Enum.join("\n")
      assert result == @one_block
    end

    test "two blox, sorted" do
      result = subject(@two_blox |> String.split("\n"))
      assert result == [
        "  # This is function a",
        "  # with no arguments",
        "  def a",
        "    42",
        "  end",
        "",
        "  # This is function b",
        "  def b; 41 end",
        ""
      ]
    end
  end

  @def_rgx ~r{\A *def}
  @comment_rgx ~r{\A *#}
  defp subject(lines) do
    sort_blox(@def_rgx, @def_rgx, @comment_rgx, lines)
  end

end
# SPDX-License-Identifier: Apache-2.0
