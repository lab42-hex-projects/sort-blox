defmodule Test.Acceptance.ByIndentTest do
  use ExUnit.Case

  import CliexSortBlox, only: [sort_blox: 4]

  @input """
  Metrics/PerceivedComplexity:
    Enabled: false

  Naming/RescuedExceptionsVariableName:
    Enabled: false

  Metrics/MethodLength:
    Max: 15
    Exclude:
      - "app/controllers/api/v1/companies_controller.rb"
      - "app/controllers/api/v2/*.rb"
      - "app/services/clients/**/*"
      - "app/services/v2/uploads/creator.rb"
      - "db/migrate/**"
      - "spec/**/*"
  """

  @output """
  Metrics/MethodLength:
    Max: 15
    Exclude:
      - "app/controllers/api/v1/companies_controller.rb"
      - "app/controllers/api/v2/*.rb"
      - "app/services/clients/**/*"
      - "app/services/v2/uploads/creator.rb"
      - "db/migrate/**"
      - "spec/**/*"

  Metrics/PerceivedComplexity:
    Enabled: false

  Naming/RescuedExceptionsVariableName:
    Enabled: false
  """
  test "sort my cops" do
    assert subject(@input) == @output |> String.split("\n")
  end

  defp subject(input) do
    indent = ~r{\A\S}
    sort_blox(indent, indent, nil, input |> String.split("\n"))
  end
end

# SPDX-License-Identifier: Apache-2.0
