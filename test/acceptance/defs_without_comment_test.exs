defmodule Test.Acceptance.DefsWithoutCommentTest do
  use ExUnit.Case

  import CliexSortBlox, only: [sort_blox: 4]

  describe "no output" do
    test "because no input" do
      result = subject([])
      assert result == []
    end

    test "because no def" do
      result = subject(~w[alpha beta])
      assert result == []
    end
  end

  @one_block """
    def a
      42
    end

  """

  @two_blox """
    def b; 41 end

    def a
      42
    end
  """
  describe "only one block" do
    test "no sorting" do
      result = subject(@one_block |> String.split("\n")) |> Enum.join("\n")
      assert result == @one_block
    end

    test "two blox, sorted" do
      result = subject(@two_blox |> String.split("\n"))
      assert result == [
        "  def a",
        "    42",
        "  end",
        "",
        "  def b; 41 end",
        ""
      ]
    end
  end

  @def_rgx ~r{\A *def}
  defp subject(lines) do
    sort_blox(@def_rgx, @def_rgx, nil, lines)
  end

end
# SPDX-License-Identifier: Apache-2.0
